## Steam Download Watcher

This tool will observe the Steam downloads and detect and execute an action, you can select, when all downloads are finished.

## Motivation

This tool was created because Steam does not have this functionality by default and you would have to leave your PC running and check yourself if the downloads are finished.

## Installation

There is no Installation required. Just head over to the [**Downloads section**](https://bitbucket.org/HellGate/steam-download-watcher/downloads) and get the newest executable.

## Contributors

You can contribute by creating pull requests. I will merge then when they fit the requirements (please use a code formatter like code maid).

## License

This project is under the MIT license. The full license can be found in the project files.

## Changelog

* v0.1 - Initial working version
* v0.2 - Added Abort Window, allows Custom Commands
* v0.3 - Added Minimize to Tray, added About Dialog