﻿using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Threading;

namespace SteamDownloadWatcher.Windows {

    /// <summary>
    /// Interaction logic for CountdownWindow.xaml
    /// </summary>
    public partial class CountdownWindow : Window {
        private DispatcherTimer _timer;
        private int _coundown = 60;
        private bool _internalClose = false;

        public CountdownWindow() {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e) {
            _timer = new DispatcherTimer();
            _timer.Interval = TimeSpan.FromSeconds(1);
            _timer.Tick += CountdownTick;
            _timer.Start();
        }

        private void CountdownTick(object sender, EventArgs e) {
            _coundown--;

            CountdownLabel.Content = _coundown.ToString();
            this.Title = string.Format("{0} - Download Finished Countdown", _coundown);

            if (_coundown == 0) {
                _timer.Stop();
                _internalClose = true;
                DialogResult = true;
            }
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e) {
            Abort();
        }

        private void Window_Closing(object sender, CancelEventArgs e) {
            Abort();
        }

        private void Abort() {
            _timer.Stop();
            if (!_internalClose)
                DialogResult = false;
        }
    }
}