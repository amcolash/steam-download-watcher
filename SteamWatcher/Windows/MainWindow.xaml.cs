﻿using SteamDownloadWatcher.Classes;
using SteamDownloadWatcher.Classes.DownloadFinishActions;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Timers;
using System.Windows;

using WForms = System.Windows.Forms;

namespace SteamDownloadWatcher.Windows {

    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window, INotifyPropertyChanged {

        #region Property Changed

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged(string propertyName) {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion Property Changed

        private DirectoryInfo _downloadsDirectory;

        private ObservableCollection<DownloadFinishAction> _downloadFinishedActions = new ObservableCollection<DownloadFinishAction>();
        public ObservableCollection<DownloadFinishAction> DownloadFinishedActions {
            get { return _downloadFinishedActions; }
            set {
                _downloadFinishedActions = value;
                OnPropertyChanged("DownloadFinishedActions");
            }
        }

        private ObservableCollection<AppInfo> _downloadingApps = new ObservableCollection<AppInfo>();
        public ObservableCollection<AppInfo> DownloadingApps {
            get { return _downloadingApps; }
            set {
                _downloadingApps = value;
                OnPropertyChanged("DownloadingApps");
            }
        }

        private bool _downloading;
        public bool Downloading {
            get { return _downloading; }
            set {
                if (value != _downloading) {
                    _downloading = value;
                    OnPropertyChanged("Downloading");
                    Application.Current.Dispatcher.Invoke((Action)(() => { DownloadChanged(); }));
                }
            }
        }

        private WForms.NotifyIcon _notify;
        private WForms.MenuItem _enableItem;

        public MainWindow() {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e) {
            _downloadsDirectory = new DirectoryInfo(SteamInfo.SteamDownloadsPath);

            // Download Finished Actions
            DownloadFinishedActions.Add(new ShutDownAction());
            DownloadFinishedActions.Add(new LogOutAction());
            DownloadFinishedActions.Add(new LockComputerAction());
            DownloadFinishedActions.Add(new HibernateAction());
            DownloadFinishedActions.Add(new CustomAction());

            // Tray Icon
            _notify = new WForms.NotifyIcon();
            _notify.Text = "Steam Download Watcher";
            _notify.Icon = new System.Drawing.Icon(Application.GetResourceStream(new Uri("pack://application:,,,/Resources/logo.ico")).Stream);
            _notify.DoubleClick += (se, ev) => { WindowState = WindowState.Normal; };
            _notify.ContextMenu = new WForms.ContextMenu();
            _enableItem = new WForms.MenuItem("Click to Enable");
            _enableItem.Click += EnableItem_Click;
            _notify.ContextMenu.MenuItems.Add(_enableItem);
            _notify.Visible = false;

            // Check Timer
            Timer t = new Timer(TimeSpan.FromSeconds(10).TotalMilliseconds);
            t.Elapsed += UpdateDownloads;
            t.Start();
            UpdateDownloads(this, null);
        }

        private void EnableItem_Click(object sender, EventArgs e) {
            ChangeStateButton.IsChecked = !ChangeStateButton.IsChecked;
            ChangeStateButton_Clicked(this, null);
            _enableItem.Text = GetStateString();
        }

        private void ChangeStateButton_Clicked(object sender, RoutedEventArgs e) {
            ChangeStateButton.Content = GetStateString();
        }

        private string GetStateString() {
            return ((ChangeStateButton.IsChecked == true) ? "Click to Disable" : "Click to Enable");
        }

        #region Steam Download Checking

        private void DownloadChanged() {
            if (!Downloading && ChangeStateButton.IsChecked == true) {
                CountdownWindow w = new CountdownWindow();
                bool? res = w.ShowDialog();
                if (res == true) {
                    DownloadFinishAction action = (DownloadFinishAction)ActionComboBox.SelectedItem;
                    action.Execute(ArgumentTextBox.Text);
                }
                Environment.Exit(0);
            }
        }

        private void UpdateDownloads(object sender, ElapsedEventArgs e) {
            DirectoryInfo[] directorys = _downloadsDirectory.GetDirectories();

            ObservableCollection<AppInfo> newlist = new ObservableCollection<AppInfo>();
            int id;
            bool downloading = false;

            // Copy all still running download AppInfo's to a new collection and add new ones (new collection to remove old ones)
            foreach (var dir in directorys) {
                // We dont know what steam is doing there so check it
                if (!int.TryParse(dir.Name, out id))
                    continue;

                downloading = true;

                IEnumerable<AppInfo> search = _downloadingApps.Where(app => app.SteamId == id);
                AppInfo existingapp;
                if (search.Count() == 0) {
                    existingapp = new AppInfo(id);
                } else {
                    existingapp = search.First();
                }
                newlist.Add(existingapp);
            }

            DownloadingApps = newlist;
            Downloading = downloading;
        }

        #endregion Steam Download Checking

        private void AboutButton_Click(object sender, RoutedEventArgs e) {
            new AboutWindow().ShowDialog();
        }

        private void Window_StateChanged(object sender, EventArgs e) {
            bool minimized = (WindowState == WindowState.Minimized);
            _notify.Visible = minimized;
            this.ShowInTaskbar = !minimized;
        }
    }
}