using Microsoft.Win32;
using System;
using System.IO;
using System.Windows.Forms;
using System.Drawing;

namespace SteamDownloadWatcher.Classes {

    public static class SteamInfo {

        /// <summary>
        /// Steam Path from the Registry
        /// </summary>
        public static string SteamPath { get; private set; }

        /// <summary>
        /// The \steamapps Path
        /// </summary>
        public static string SteamAppsPath {
            get {
                return Path.Combine(SteamPath, "steamapps");
            }
        }

        /// <summary>
        /// The \steamapps\downloading Path
        /// </summary>
        public static string SteamDownloadsPath {
            get {
                return Path.Combine(SteamAppsPath, "downloading");
            }
        }

        /// <exception cref="System.Exception">Thrown when the RegistryKey was not found</exception>
        static SteamInfo() {
            RegistryKey regKey = Registry.CurrentUser;
            regKey = regKey.OpenSubKey(@"Software\Valve\Steam");

            if (regKey != null) {
                SteamPath = Path.GetFullPath(regKey.GetValue("SteamPath").ToString());
            } else {
                throw new Exception("SteamPath Registry Key not found");
            }

            /* Confirm path with a dialog box before continuing */
            /* Code modified from http://www.csharp-examples.net/inputbox/ */
            Form form = new Form();
            TextBox textBox = new TextBox();
            Button buttonOk = new Button();

            form.Text = "Enter your steam path here if not default location";
            textBox.Text = SteamPath;
            buttonOk.Text = "OK";
            buttonOk.DialogResult = DialogResult.OK;

            textBox.SetBounds(12, 36, 372, 20);
            buttonOk.SetBounds(309, 72, 75, 23);

            textBox.Anchor = textBox.Anchor | AnchorStyles.Right;
            buttonOk.Anchor = AnchorStyles.Bottom | AnchorStyles.Right;

            form.ClientSize = new Size(396, 107);
            form.Controls.AddRange(new Control[] { textBox, buttonOk  });

            form.FormBorderStyle = FormBorderStyle.FixedDialog;
            form.StartPosition = FormStartPosition.CenterScreen;
            form.MinimizeBox = false;
            form.MaximizeBox = false;
            form.AcceptButton = buttonOk;

            DialogResult dialogResult = form.ShowDialog();

            SteamPath = textBox.Text;
            /* End dialogue */

        }
    }
}
