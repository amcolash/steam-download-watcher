﻿namespace SteamDownloadWatcher.Classes.DownloadFinishActions {

    public class DownloadFinishAction {

        public virtual string Name { get { return "Undefined"; } }

        public virtual void Execute(string argument = "") {
        }
    }
}