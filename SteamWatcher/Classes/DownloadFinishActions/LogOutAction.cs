﻿using System.Diagnostics;

namespace SteamDownloadWatcher.Classes.DownloadFinishActions {

    public class LogOutAction : DownloadFinishAction {

        public override string Name { get { return "Log Out"; } }

        public override void Execute(string argument = "") {
            InteropFunctions.ExitWindowsEx(0, 0);
        }
    }
}