﻿namespace SteamDownloadWatcher.Classes.DownloadFinishActions {

    public class HibernateAction : DownloadFinishAction {

        public override string Name { get { return "Hibernate"; } }

        public override void Execute(string argument = "") {
            InteropFunctions.SetSuspendState(true, true, true);
        }
    }
}