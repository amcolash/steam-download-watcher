﻿namespace SteamDownloadWatcher.Classes.DownloadFinishActions {

    public class LockComputerAction : DownloadFinishAction {

        public override string Name { get { return "Lock Computer"; } }

        public override void Execute(string argument = "") {
            InteropFunctions.LockWorkStation();
        }
    }
}