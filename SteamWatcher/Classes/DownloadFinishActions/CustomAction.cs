﻿using System;
using System.Diagnostics;
using System.Windows;

namespace SteamDownloadWatcher.Classes.DownloadFinishActions {

    public class CustomAction : DownloadFinishAction {

        public override string Name { get { return "CMD call to [Argument]"; } }

        public override void Execute(string argument) {
            try {
                // Start cmd with /c parameter so it will execute the argument string
                ProcessStartInfo procStartInfo = new ProcessStartInfo("cmd", "/c " + argument);
                procStartInfo.CreateNoWindow = true;

                // Create a process from the ProcessStartInfo and start it
                Process proc = new Process();
                proc.StartInfo = procStartInfo;
                proc.Start();
            } catch (Exception e) {
                MessageBox.Show(string.Format("Failed to run custom Commad:\n\n{0}", e.Message));
            }
        }
    }
}