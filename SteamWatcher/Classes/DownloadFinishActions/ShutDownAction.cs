﻿using System.Diagnostics;

namespace SteamDownloadWatcher.Classes.DownloadFinishActions {

    public class ShutDownAction : DownloadFinishAction {

        public override string Name { get { return "Shut Down"; } }

        public override void Execute(string argument = "") {
            Process.Start("shutdown", "/s /t 0");
        }
    }
}