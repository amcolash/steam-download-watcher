﻿using System.ComponentModel;

namespace SteamDownloadWatcher.Classes {

    public class AppInfo : INotifyPropertyChanged {

        #region Property Changed

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged(string propertyName) {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion Property Changed

        private int _steamId;
        public int SteamId {
            get { return _steamId; }
            set {
                _steamId = value;
                OnPropertyChanged("SteamId");
            }
        }

        // TODO: async ask steam api for the name: http://store.steampowered.com/api/appdetails?appids={SteamId}
        /*private int _name;
        public int Name {
            get { return _name; }
            set {
                _name = value;
                OnPropertyChanged("Name");
            }
        }*/

        public AppInfo(int steamid) {
            SteamId = steamid;
        }
    }
}